def test_get(idem_cli):
    ret = idem_cli("exec", "gitlab.metadata.get")

    # Verify that the subprocess succeeded
    assert ret["result"], ret["stderr"]
    assert ret["json"], "Output did not result in readable json"

    # Verify the output of the exec module
    get_ret = ret["json"]
    assert get_ret["result"], get_ret["comment"]
    assert get_ret["ret"]
