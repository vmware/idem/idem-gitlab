import uuid

import pytest


@pytest.fixture(scope="module")
async def gitlab_test_group(hub, ctx):
    """
    A fixture that provides a test gitlab group
    """
    create = await hub.exec.gitlab.group.create(
        ctx, path=f"idem-gitlab-test-fixture-group-{uuid.uuid4()}"
    )
    assert create.result, create.comment

    resource_id = create.ret.resource_id

    yield create.ret

    delete = await hub.exec.gitlab.group.delete(ctx, resource_id=resource_id)
    assert delete.result, delete.comment


@pytest.fixture(scope="module")
async def gitlab_test_project(hub, ctx):
    """
    A fixture that provides a test gitlab project
    """
    create = await hub.exec.gitlab.project.create(
        ctx, path=f"idem-gitlab-test-fixture-project-{uuid.uuid4()}"
    )
    assert create.result, create.comment

    resource_id = create.ret.resource_id

    yield create.ret

    delete = await hub.exec.gitlab.project.delete(ctx, resource_id=resource_id)
    assert delete.result, delete.comment


@pytest.fixture(scope="module")
async def gitlab_test_commit(hub, ctx, gitlab_test_project):
    """
    A fixture that provides a commit into a gitlab project
    """
    project_id = gitlab_test_project.resource_id
    data = dict(
        branch="main",
        commit_message="test: initial_commit",
        actions=[
            dict(action="create", file_path="README.md", content="asdff"),
        ],
    )
    create = await hub.tool.gitlab.request.json(
        ctx,
        "post",
        success_codes=[201, 304, 204],
        url=f"{ctx.acct.endpoint_url}/projects/{project_id}/repository/commits",
        data=data,
    )
    assert create.result, create.comment

    create.ret = hub.tool.gitlab.project.commit.raw_to_present(create.ret)
    create.ret["project_id"] = project_id

    yield create.ret


@pytest.fixture(scope="module")
async def gitlab_test_user(hub, ctx):
    """
    A fixture that provides a test gitlab user
    """
    create = await hub.exec.gitlab.user.create(
        ctx, email=f"gitlab-test-fixture-user-{uuid.uuid4()}@example.com"
    )
    assert create.result, create.comment

    resource_id = create.ret.resource_id

    yield create.ret

    delete = await hub.exec.gitlab.user.delete(ctx, resource_id=resource_id)
    assert delete.result, delete.comment
